//
//  FDUserAvatarCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDUserAvatarCell.h"

@implementation FDUserAvatarCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _avatar = [UIImageView new];
        _avatar.clipsToBounds      = YES;
        _avatar.backgroundColor    = ColorNormalPlaceholder;
        _avatar.layer.borderColor  = ColorNormalBGWhite.CGColor;
        _avatar.layer.borderWidth  = 0.5;
        _avatar.layer.cornerRadius = 20;
        
        _nameLabel = [UILabel labelWithText:@"头像" Color:ColorCellText FontSize:14 Alignment:NSTextAlignmentLeft Light:YES];
        
        ContentViewAddSubView(_avatar);
        ContentViewAddSubView(_nameLabel);
        
        self.line.hidden  = YES;
        
        [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@18);
            make.top.equalTo(@25);
        }];
        
        [_avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.size.mas_equalTo(CGSizeMake(40, 40));
            make.right.equalTo(@(-35));
            make.centerY.equalTo(@0);
        }];
    }
    return self;
}

@end
