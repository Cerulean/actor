//
//  FDDefaultInfoView.m
//  actor
//
//  Created by 王澍宇 on 16/4/6.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDDefaultInfoView.h"

@implementation FDDefaultInfoView {
    UIImageView *_backgroundView;
    
    UIImageView *_imageView;
    
    UILabel *_headLabel;
    
    UILabel *_tipLabel;
    
    FDButton *_loginButton;
}

- (instancetype)initWithHeadText:(NSString *)headText TipText:(NSString *)tipText Image:(UIImage *)image BackgounrdImage:(UIImage *)backgroundImage {
    
    if (self = [super initWithShadowColor:[UIColor clearColor] Info:nil]) {
        
        self.cancelButton.hidden = YES;
        self.titleLabel.hidden = YES;
        self.firstLine.hidden = YES;
        
        _backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
        _backgroundView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview:_backgroundView];
        
        _headLabel = [UILabel labelWithText:headText Color:ColorCellText FontSize:20 Alignment:NSTextAlignmentCenter Light:NO];
        
        _tipLabel  = [UILabel labelWithText:tipText Color:ColorCellText FontSize:14 Alignment:NSTextAlignmentCenter Light:NO];
        
        _imageView = [[UIImageView alloc] initWithImage:image];
        
        _loginButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"去发现" FontSize:18 ActionBlock:^(FDButton *button) {
            
            FDTabBarController *tabBarController = (FDTabBarController *)[UIApplication sharedApplication].keyWindow.rootViewController;
            
            if ([tabBarController isKindOfClass:[FDTabBarController class]]) {
                
                FDButton *button = [[tabBarController buttons] firstObject];
                button.tapAction(button);
                
            }
            
        }];
        
        [_loginButton setTitleColor:ColorNormalBGWhite forState:UIControlStateNormal];
        [_loginButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:NO AutoSelected:NO];
        
        ContentViewAddSubView(_headLabel);
        ContentViewAddSubView(_imageView);
        ContentViewAddSubView(_tipLabel);
        ContentViewAddSubView(_loginButton);
        
        [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(@0);
        }];
        
        [_headLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(@20);
        }];
        
        [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(_headLabel.mas_bottom).offset(10);
        }];
        
        [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(_imageView.mas_bottom).offset(10);
        }];
        
        [_loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(_tipLabel.mas_bottom).offset(15);
            make.left.equalTo(@55);
            make.right.equalTo(@(-55));
            make.bottom.equalTo(@(-25));
        }];
        
        [self bringSubviewToFront:self.shadowView];
    }
    return self;
}

@end
