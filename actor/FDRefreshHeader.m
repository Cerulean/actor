//
//  FDRefreshHeader.m
//  maruko
//
//  Created by 王澍宇 on 16/2/23.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDRefreshHeader.h"

@implementation FDRefreshHeader {
    UIView *_circleContentView;
    UIImageView *_circle;
}

- (void)prepare {
    [super prepare];
    
    self.mj_h = 50;
    
    _circle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Normal_Loading"]];
    
    _circleContentView = [UIView new];
    
    [self addSubview:_circleContentView];
    [_circleContentView addSubview:_circle];
}

- (void)placeSubviews {
    [super placeSubviews];
    
    _circleContentView.center  = CGPointMake(self.center.x, self.mj_h - 30);
    _circleContentView.mj_size = CGSizeMake(20, 20);
    
    _circle.mj_size = CGSizeMake(20, 20);
    _circle.bounds  = _circleContentView.bounds;
}

- (void)setState:(MJRefreshState)state {
    
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            [self stopRotation];
            break;
        case MJRefreshStatePulling:
            break;
        case MJRefreshStateRefreshing:
            [self startRotation];
            break;
        default:
            break;
    }
}

- (void)startRotation {
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = 1.5;
    animation.fillMode = kCAFillModeBoth;
    animation.repeatCount = HUGE_VALF;
    animation.removedOnCompletion = NO;
    
    CGFloat currentRotation = acosf(_circle.layer.affineTransform.a);
    
    animation.fromValue = [NSNumber numberWithFloat:currentRotation];
    animation.toValue = [NSNumber numberWithFloat:currentRotation + 2 * M_PI];
    
    [_circle.layer setSpeed:1.0];
    [_circle.layer addAnimation:animation forKey:@"rotate-layer"];
}

- (void)stopRotation {
    
    [_circle.layer removeAllAnimations];
    
}

- (void)setPullingPercent:(CGFloat)pullingPercent {
    [super setPullingPercent:pullingPercent];
    
    if ([_circle.layer animationKeys].count) {
        return;
    }
    
    _circle.layer.affineTransform = CGAffineTransformMakeRotation(pullingPercent * M_PI);
}

@end
