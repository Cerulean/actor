//
//  FDProjectViewController.h
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseViewController.h"
#import "FDProjectService.h"
#import "FDProject.h"
#import "FDTopic.h"

@interface FDProjectViewController : FDBaseViewController

@property (nonatomic, strong) FDTopic *topic;

@end
