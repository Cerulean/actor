//
//  LayoutSheet.h
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#ifndef LayoutSheet_h
#define LayoutSheet_h

#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define STATUS_HEIGHT 20
#define TABBAR_HEIGHT 50
#define COMMON_HEIGHT 44

#define MAIN_COLLECTION_CELL_HEIGHT  70
#define MAIN_COLLECTION_CELL_WIDTH   50
#define MAIN_COLLECTION_CELL_SPACING 20
#define MAIN_COLLECTION_CELL_EDGE    15

#endif /* LayoutSheet_h */
