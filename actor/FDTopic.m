//
//  FDTopic.m
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTopic.h"

@implementation FDTopic

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"objectID"  : @"_id",
             @"title"     : @"title",
             @"intro"     : @"desc",
             @"imageURL"  : @"image_url",
             @"tag"       : @"tag",
             @"projects"  : @"projects",
             };
}

+ (NSValueTransformer *)tagJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[FDTag class]];
}

+ (NSValueTransformer *)projectsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[FDProject class]];
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"";
        self.intro = @"";
    }
    return self;
}

@end
