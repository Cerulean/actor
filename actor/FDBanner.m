//
//  FDBanner.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBanner.h"

@implementation FDBanner

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"imageURL"     : @"image_url",
             @"linkURL"      : @"link_url",
             };
}

@end
