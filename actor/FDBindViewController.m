//
//  FDBindViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/7.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBindViewController.h"

@implementation FDBindViewController {
    
    UITextField *_telField;
    UITextField *_pwdField;
    
    FDButton *_confirmButton;
    
    UIImageView *_backgroundView;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"绑定账号";
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self updateNaviBarType:FDNaviBarTypeClear];
    
    WeakSelf;
    
    _backgroundView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Register_BG")];
    
    _telField = [UITextField new];
    _telField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _telField.textColor = ColorAccountTextMain;
    _telField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入手机号" Color:ColorAccountPlacehodler];
    _telField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _telField.leftViewMode = UITextFieldViewModeAlways;
    _telField.keyboardType = UIKeyboardTypeNamePhonePad;
    _telField.background = FDImageWithName(@"Account_Field_Border");
    
    _pwdField = [UITextField new];
    _pwdField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _pwdField.textColor = ColorAccountTextMain;
    _pwdField.secureTextEntry = YES;
    _pwdField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入密码" Color:ColorAccountPlacehodler];
    _pwdField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _pwdField.leftViewMode = UITextFieldViewModeAlways;
    _pwdField.background = FDImageWithName(@"Account_Field_Border");
    _pwdField.editingAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self->_confirmButton setEnabled:text.length ? YES : NO];
    };
    
    _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"确定" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        NSString *tel = s_self->_telField.text;
        NSString *pwd = s_self->_pwdField.text;
        
        if ([s_self checkIfPwdLegal:pwd]) {
            
            [FDAccountService bindSSOWithUsername:tel Password:pwd SsoID:s_self.ssoID SsoType:s_self.ssoType Callback:^(BOOL success) {
                if (success) {
                    [s_self dismissViewControllerAnimated:YES completion:nil];
                }
            }];
            
        }
        
    }];
    
    [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:YES AutoSelected:NO];
    [_confirmButton setTitleColor:ColorAccountTextMain forState:UIControlStateNormal];
    [_confirmButton setEnabled:NO];
    
    [self.view addSubview:_backgroundView];
    [self.view addSubview:_telField];
    [self.view addSubview:_pwdField];
    [self.view addSubview:_confirmButton];
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
    }];
    
    [_telField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(SCREEN_HEIGHT / 4));
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@40);
    }];
    
    [_pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_telField.mas_bottom).offset(10);
        make.left.equalTo(_telField);
        make.right.equalTo(_telField);
        make.height.equalTo(_telField);
    }];
    
    [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_pwdField.mas_bottom).offset(30);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@45);
    }];
}
    

- (BOOL)checkIfPwdLegal:(NSString *)pwd {
    
    if (![FDValidater validatePassword:pwd]) {
        [FDAlert alertWithTitle:@"错误" Message:@"密码长度不合法"];
        return NO;
    }
    
    return YES;
}

@end
