//
//  FDOrderService.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDOrderService.h"

static NSMutableDictionary *_applyParms;

@implementation FDOrderService

+ (void)load {
    _applyParms = [NSMutableDictionary new];
}

+ (NSMutableDictionary *)applyParms {
    return _applyParms;
}

+ (void)checkoutFee:(CGFloat)fee Type:(FDOrderCheckoutType)type WithCallback:(void(^)(BOOL success, NSString *orderID))callback {
    
    NSString *api = type == FDOrderCheckoutTypeApplyFee ? @"order/apply" : @"order/checkout";
    
    [FDWebService requestWithAPI:api Method:@"POST" Parms:_applyParms HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        if (callback) {
            
            if (success) {
                
                if (fee > 0) {
                    
                    NSString *channel = _applyParms[@"channel"];
                    
                    NSString *urlScheme = [channel isEqualToString:kChannelWechat] ? kURLSchemeWechat : kURLSchemeAlipay;
                    
                    [Pingpp createPayment:resultDic
                           viewController:[UIApplication sharedApplication].keyWindow.rootViewController
                             appURLScheme:urlScheme
                           withCompletion:^(NSString *result, PingppError *error) {
                               
                               if ([result isEqualToString:@"success"]) {
                                   
                                   [SVProgressHUD showSuccessWithStatus:@"支付成功"];
                                   
                                   NSString *orderNo = resultDic[@"order_no"];
                                   
                                   callback(YES, [orderNo substringFromIndex:2]);
                                   
                                   _applyParms = [NSMutableDictionary new];
                                   
                                   [[NSNotificationCenter defaultCenter] postNotificationName:kCheckoutSuccessNotification object:nil];
                                   
                               } else {
                                   
                                   [SVProgressHUD showErrorWithStatus:[error getMsg]];
                                   
                                   callback(NO, nil);
                               }
                           }];
                } else {
                    
                    NSString *info = type == FDOrderCheckoutTypeApplyFee ? @"报名成功" : @"申请成功";
                    
                    [SVProgressHUD showSuccessWithStatus:info];
                    
                    callback(YES, resultDic[@"_id"]);
                    
                    _applyParms = [NSMutableDictionary new];
                }
                
            } else {
                callback(NO, nil);
            }
        }
    }];
}

+ (void)refundWithOrderID:(NSString *)orderID Callback:(FDWebServiceCallback)callback {
    
    [FDWebService requestWithAPI:@"order/refund" Method:@"POST" Parms:@{@"order_id" : orderID} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            callback(success);
        }
        
    }];
    
}

+ (void)getCheckoutWithOrderID:(NSString *)orderID Callback:(void(^)(BOOL success, FDCheckout *checkout))callback {
    
    [FDWebService requestWithAPI:@"order/checkout" Method:@"GET" Parms:@{@"order_id" : orderID} HUD:YES Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            callback(success, [MTLJSONAdapter modelOfClass:[FDCheckout class] fromJSONDictionary:resultDic error:nil]);
        }
        
    }];
}

@end
