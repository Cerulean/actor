//
//  FDAccountService.h
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import <WeiboSDK.h>

#import "FDWebService.h"
#import "FDUser.h"
#import "WXApi.h"

typedef enum : NSUInteger {
    FDLoginPlatformTypeWechat,
    FDLoginPlatformTypeWeibo,
} FDLoginPlatformType;

@interface FDAccountService : FDWebService

+ (FDUser *)currentUser;

+ (BOOL)checkIfNeedLogin;

+ (void)promptLogin;

+ (void)showLoginController;

+ (void)loginWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (BOOL)canLoginWithWechat;

+ (void)purposeLoginWithPlatform:(FDLoginPlatformType)type;

+ (void)loginWithPlatform:(FDLoginPlatformType)type Parms:(NSDictionary *)parms;

+ (void)bindSSOWithUsername:(NSString *)username
                   Password:(NSString *)password
                      SsoID:(NSString *)ssoID
                    SsoType:(FDLoginPlatformType)type
                   Callback:(FDWebServiceCallback)callback;

+ (void)logoutWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (void)registerWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (void)getSMSCodeWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (void)forgetPasswordWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (void)getInfoWithParms:(NSDictionary *)parms Callback:(void (^)(FDUser *user))callback;

+ (void)modifyInfoWithParms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

+ (void)changePwdWithOldPwd:(NSString *)oldPwd NewPwd:(NSString *)newPwd Callback:(void (^)(BOOL success))callback;

+ (void)updateAvatarWithImage:(UIImage *)image Parms:(NSDictionary *)parms Callback:(void (^)(BOOL success))callback;

@end
