//
//  FDSettingViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/18.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDSettingViewController.h"
#import "FDWebViewController.h"
#import "FDPwdViewController.h"
#import "FDAccountService.h"

@interface FDSettingViewController ()

@end

@implementation FDSettingViewController

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"设置";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableViewCell *pwdCell    = [self generateCellWithTitle:@"修改密码"];
    UITableViewCell *termCell   = [self generateCellWithTitle:@"服务条款"];
    UITableViewCell *logoutCell = [self generateCellWithTitle:@"退出登录"];
    
    self.cellsArray = [@[@[pwdCell], @[termCell], @[logoutCell]] mutableCopy];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        
        FDPwdViewController *pwdController = [FDPwdViewController new];
        
        FDNavigationController *naviController = [[FDNavigationController alloc] initWithRootViewController:pwdController];
        
        [self presentViewController:naviController animated:YES completion:nil];
    } else if (indexPath.section == 1) {
        
        FDWebViewController *webController = [FDWebViewController new];
        
        webController.linkURL = kURLDuty;
        
        [self.navigationController pushViewController:webController animated:YES];
        
    } else {
        
        [FDAlert alertWithTitle:@"退出登录" Message:nil Stressed:YES confirmAction:^{
            [FDAccountService logoutWithParms:nil Callback:^(BOOL success) {
                if (success) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }];
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 9;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

@end
