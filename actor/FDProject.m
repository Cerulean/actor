//
//  FDProject.m
//  actor
//
//  Created by 王澍宇 on 16/3/22.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDProject.h"

@implementation FDProject

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objectID"       : @"_id",
             @"title"          : @"title",
             @"intro"          : @"desc",
             @"tags"           : @"tags",
             @"applyPlace"     : @"apply_place",
             @"applyFee"       : @"apply_fee",
             @"packages"       : @"packages",
             @"imageURLs"      : @"image_urls",
             @"departureTimes" : @"departure_times",
             @"routeIntro"     : @"route_desc",
             @"feeIntro"       : @"fee_desc",
             @"bookIntro"      : @"book_desc",
             @"cancelIntro"    : @"cancel_desc",
             @"qaIntro"        : @"qas",
             @"hasFavoed"      : @"has_favoed"
             };
}

+ (NSValueTransformer *)packagesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[FDPackage class]];
}

+ (NSValueTransformer *)departureTimesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[FDDepartureTime class]];
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"";
        self.intro = @"";
    }
    return self;
}


@end
