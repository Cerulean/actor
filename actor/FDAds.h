//
//  FDAds.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseModel.h"

@interface FDAds : FDBaseModel

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) NSString *linkURL;

@property (nonatomic, strong) NSString *imageURL;

@end
