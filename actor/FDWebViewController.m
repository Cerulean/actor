//
//  FDWebViewController.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDWebViewController.h"

@implementation FDWebViewController {
    
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"浏览器";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeWhite];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    _webView.scalesPageToFit = YES;
    _webView.delegate = self;
    
    [self.view addSubview:_webView];
    
    if (_linkURL) {
        
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_linkURL]]];
        
    } else if (_htmlString) {
        
        [_webView loadHTMLString:_htmlString baseURL:nil];
    } else {
        
    }
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    self.title = title.length ? title : @"浏览器";
}


@end
