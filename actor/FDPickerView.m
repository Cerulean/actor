//
//  CPPickerView.m
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "FDPickerView.h"

#import "FDBaseViewController.h"

@interface FDPickerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation FDPickerView {
    __weak FDBaseViewController *_viewController;
    
    UIView *_contentView;
    
    UIView *_topLine;
    
    BOOL _hasShown;
}

- (instancetype)initWithObjects:(NSArray *)array containerController:(FDBaseViewController *)controller {
    
    if (self = [super init]) {
        
        _pickerObjects = [array copy];
        
        _viewController = controller;
        
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        self.backgroundColor = [UIColor clearColor];
        
        WeakSelf;
        
        _contentView = [[UIView alloc] init];
        _contentView.bounds = CGRectMake(0, 0, SCREEN_WIDTH, 250);
        _contentView.backgroundColor = ColorTableSection;
        
        _cancelButton = [FDButton buttonWithType:UIButtonTypeSystem ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            [s_self hide];
        }];
        
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton.titleLabel setFont:[UIFont systemFontOfSize:17.0]];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        _finishButton = [FDButton buttonWithType:UIButtonTypeSystem ActionBlock:^(FDButton *button) {
            
            StrongSelf;
            
            NSMutableArray *objects = [NSMutableArray new];
            
            for (NSInteger component = 0; component < s_self.pickerView.numberOfComponents; ++component) {
                NSInteger row = [s_self.pickerView selectedRowInComponent:component];
                
                NSArray *rowArray = [s_self.pickerObjects objectAtIndex:component];
                
                if (rowArray.count > 0) {
                    [objects addObject:rowArray[row]];
                }
            }
            
            if (s_self.callback) {
                s_self.callback([objects copy]);
            }
            
            [s_self hide];
        }];
        
        [_finishButton setTitle:@"完成" forState:UIControlStateNormal];
        [_finishButton.titleLabel setFont:[UIFont systemFontOfSize:17.0]];
        [_finishButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.dataSource = self;
        _pickerView.delegate   = self;
        
        _topLine = [UIView new];
        _topLine.backgroundColor = ColorNormalPlaceholder;
        
        [_contentView addSubview:_topLine];
        [_contentView addSubview:_cancelButton];
        [_contentView addSubview:_finishButton];
        [_contentView addSubview:_pickerView];
        
        [_topLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
        
        [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@10);
            make.top.equalTo(@10);
            make.size.mas_equalTo(CGSizeMake(80, 30));
        }];
        
        [_finishButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(@(-10));
            make.top.equalTo(_cancelButton);
            make.size.mas_equalTo(_cancelButton);
        }];
        
        [_pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_cancelButton.mas_bottom);
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.bottom.equalTo(@0);
        }];
        
        [_viewController.view addSubview:self];
        
        [self addSubview:_contentView];
        [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_bottom);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@250);
        }];
        
        [_contentView layoutIfNeeded];
        
        _hasShown = NO;
        
        self.hidden = YES;
    }
    return self;
}

- (void)show {
    
    if (_hasShown) {
        return;
    }
    
    self.hidden = NO;
    
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
        make.height.equalTo(@250);
    }];
    
    [UIView animateWithDuration:0.25 animations:^{
        [_contentView layoutIfNeeded];
    }];
    
    _hasShown = YES;
}

- (void)hide {
    
    if (!_hasShown) {
        return;
    }
    
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_bottom);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.equalTo(@250);
    }];
    
    [UIView animateWithDuration:0.25 animations:^{
        [_contentView layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
    
    _hasShown = NO;
}

- (void)setPickerObjects:(NSArray *)pickerObjects {
    _pickerObjects = [pickerObjects copy];
    [_pickerView reloadAllComponents];
}

#pragma mark - PickerView Delegate && DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return _pickerObjects.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[_pickerObjects objectAtIndex:component] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[_pickerObjects objectAtIndex:component] objectAtIndex:row];
}

@end
