//
//  FDDepartureTime.m
//  actor
//
//  Created by 王澍宇 on 16/4/3.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDDepartureTime.h"

@implementation FDDepartureTime

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"objectID"   : @"_id",
             @"startTime"  : @"start_time",
             @"vacancy"    : @"vacancies",
             @"period"     : @"desc.period",
             };
}

+ (NSValueTransformer *)objectIDJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [value stringValue];
    }];
}

+ (NSValueTransformer *)startTimeJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [FDDateFormatter dateFromString:value];
    }];
}

@end
