//
//  FDWebViewController.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseViewController.h"

@interface FDWebViewController : FDBaseViewController <UIWebViewDelegate>

@property (nonatomic, strong) NSString *linkURL;

@property (nonatomic ,strong) NSString *htmlString;

@property (nonatomic, strong) UIWebView *webView;

@end
