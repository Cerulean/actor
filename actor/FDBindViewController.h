//
//  FDBindViewController.h
//  actor
//
//  Created by 王澍宇 on 16/4/7.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseViewController.h"
#import "FDAccountService.h"

@interface FDBindViewController : FDBaseViewController

@property (nonatomic, assign) FDLoginPlatformType ssoType;

@property (nonatomic, strong) NSString *ssoID;

@end
