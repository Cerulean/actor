//
//  FDProjectWebService.h
//  actor
//
//  Created by 王澍宇 on 16/3/23.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDWebService.h"
#import "FDProject.h"
#import "FDPerson.h"

@interface FDProjectService : FDWebService

+ (void)getProjectWithObjectID:(NSString *)objectID Callback:(void(^)(BOOL success, FDProject *project))callback;

+ (void)favoProject:(NSString *)projectID Callback:(FDWebServiceCallback)callback;

+ (void)unfavoProject:(NSString *)projectID Callback:(FDWebServiceCallback)callback;

+ (void)consultWithName:(NSString *)name tel:(NSString *)tel mail:(NSString *)mail problem:(NSString *)problem Callback:(FDWebServiceCallback)callback;

@end
