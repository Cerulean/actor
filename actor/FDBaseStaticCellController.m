//
//  FDBaseStaticCellController.m
//  maruko
//
//  Created by 王澍宇 on 16/2/23.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#import "FDBaseStaticCellController.h"

@implementation FDBaseStaticCellController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.dataSource = self;
    _tableView.contentInset   = UIEdgeInsetsMake(0, 0, TABBAR_HEIGHT + STATUS_HEIGHT, 0);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = ColorTableSection;
    _tableView.delegate = self;
    
    [self.view addSubview:_tableView];
    
}

- (UITableViewCell *)generateCellWithTitle:(NSString *)title {
    
    UITableViewCell *cell    = [UITableViewCell new];
    
    cell.accessoryType       = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text      = title;
    cell.textLabel.font      = [UIFont systemFontOfSize:14 weight:UIFontWeightLight];
    cell.textLabel.textColor = ColorCellText;
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.5)];
    line.backgroundColor = ColorCellLine;
    
    [cell.contentView addSubview:line];
    
    return cell;
}

#pragma mark - UITableViewDelegate && UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cellsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cellsArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return _cellsArray[indexPath.section][indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
