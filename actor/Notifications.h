//
//  Notifications.h
//  maruko
//
//  Created by 王澍宇 on 16/2/22.
//  Copyright © 2016年 Shuyu. All rights reserved.
//

#ifndef Notifications_h
#define Notifications_h

#define kNeedLoginNotification       @"kNeedLoginNotification"
#define kLogoutNotification          @"kLogoutNotification"
#define kCheckoutSuccessNotification @"kCheckoutSuccessNotification"

#define KSSOLoginSuccessNotification  @"KSSOLoginSuccessNotification"
#define KSSOLoginNeedBindNotification @"KSSOLoginNeedBindNotification"

#endif /* Notifications_h */
