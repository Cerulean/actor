//
//  FDTextCell.m
//  actor
//
//  Created by 王澍宇 on 16/4/1.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDTextCell.h"

@implementation FDTextCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self.textLabel setTextColor:ColorCellText];
        
    }
    return self;
}

@end
