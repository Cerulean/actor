//
//  FDTripCell.h
//  actor
//
//  Created by 王澍宇 on 16/3/19.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseCell.h"
#import "FDTopic.h"

@interface FDProjectCell : FDBaseCell

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UILabel *infoLabel;

@property (nonatomic, strong) UILabel *introLabel;

@property (nonatomic, strong) FDButton *favoButton;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end
