//
//  FDUserAvatarCell.h
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDBaseCell.h"

@interface FDUserAvatarCell : FDBaseCell

@property (nonatomic, strong) UIImageView *avatar;

@property (nonatomic, strong) UILabel *nameLabel;

@end
