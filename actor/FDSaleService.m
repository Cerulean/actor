//
//  FDSaleService.m
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDSaleService.h"

@implementation FDSaleService

+ (void)getBannersWithCallback:(void(^)(NSArray<FDBanner *> *banners, BOOL success))callback {
    
    [FDWebService requestWithAPI:@"sale/banner" Method:@"GET" Parms:@{} HUD:NO Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            callback([MTLJSONAdapter modelsOfClass:[FDBanner class] fromJSONArray:resultDic[@"banners"] error:nil], success);
        }
        
    }];
    
}

+ (void)getAdsWithCallback:(void(^)(FDAds *ads, BOOL success))callback {
    
    [FDWebService requestWithAPI:@"sale/ads" Method:@"GET" Parms:@{} HUD:NO Block:^(BOOL success, NSDictionary *resultDic) {
        
        if (callback) {
            callback([MTLJSONAdapter modelOfClass:[FDAds class] fromJSONDictionary:resultDic error:nil], success);
        }
        
    }];
}

@end
