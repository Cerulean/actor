//
//  FDSettingCell.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDSettingCell.h"

@implementation FDSettingCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _icon = [UIImageView new];
        _icon.contentMode = UIViewContentModeScaleAspectFit;
        
        _titleLabel = [UILabel labelWithText:@"超级活动家" Color:ColorCellText FontSize:14 Alignment:NSTextAlignmentLeft Light:YES];
        
        ContentViewAddSubView(_icon);
        ContentViewAddSubView(_titleLabel);
        
        [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@20);
            make.top.equalTo(@15);
            make.size.mas_equalTo(CGSizeMake(16, 16));
        }];
        
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_icon.mas_right).offset(10);
            make.centerY.equalTo(_icon);
        }];
        
        [self.line mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
    }
    
    return self;
}
@end
