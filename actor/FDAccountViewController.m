//
//  FDAccountViewController.m
//  actor
//
//  Created by 王澍宇 on 16/3/17.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDAccountViewController.h"
#import "FDAccountService.h"

@interface FDAccountViewController ()

@end

@implementation FDAccountViewController {
    UIImageView *_backgroundView;
    
    UITextField *_telField;
    UITextField *_smsField;
    UITextField *_pwdField;
    
    FDButton *_forgetButton;
    FDButton *_smsButton;
    
    FDButton *_confirmButton;
    
    UIView *_forgetLine;
    
    UILabel *_forgetNoticeLabel;
    
    BOOL _isMoved;
    
    NSTimer *_timer;
    
    NSTimeInterval _interval;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNaviBar];
    [self loadSubViews];
    [self configureTimer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNaviBarType:FDNaviBarTypeClear];
}

- (void)configureTimer {
    _interval = 60;
    
    _timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    
    [_timer setFireDate:[NSDate distantFuture]];
}

- (NSString *)configureTitle {
    
    NSString *confirmText = nil;
    
    if (_logicType == FDAccountLogicTypeLogin) {
        confirmText = @"登录";
        self.title = confirmText;
    } else if (_logicType == FDAccountLogicTypeRegister) {
        confirmText = @"注册";
        self.title = confirmText;
    } else if (_logicType == FDAccountLogicTypeBind) {
        confirmText = @"绑定";
        self.title = [confirmText stringByAppendingString:@"账号"];
    } else {
        confirmText = @"确定";
        self.title = @"忘记密码";
    }
    
    return confirmText;
}

- (void)configureNaviBar {
    [self.navigationItem.leftBarButtonItem setTintColor:ColorWelcomeTextMain];
}

- (void)loadFields {
    WeakSelf;
    
    _telField = [UITextField new];
    _telField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _telField.textColor = ColorAccountTextMain;
    _telField.keyboardType = UIKeyboardTypeNumberPad;
    _telField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入手机号" Color:ColorAccountPlacehodler];
    _telField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _telField.leftViewMode = UITextFieldViewModeAlways;
    _telField.background = FDImageWithName(@"Account_Field_Border");
    _telField.beginAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self moveHeigher];
    };
    _telField.doneAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self moveBack];
    };
    
    _smsField = [UITextField new];
    _smsField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _smsField.textColor = ColorAccountTextMain;
    _smsField.keyboardType = UIKeyboardTypeNumberPad;
    _smsField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入验证码" Color:ColorAccountPlacehodler];
    _smsField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _smsField.leftViewMode = UITextFieldViewModeAlways;
    _smsField.background = FDImageWithName(@"Account_Field_Border");
    _smsField.doneAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self moveBack];
    };
    
    _pwdField = [UITextField new];
    _pwdField.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    _pwdField.textColor = ColorAccountTextMain;
    _pwdField.secureTextEntry = YES;
    _pwdField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"输入密码" Color:ColorAccountPlacehodler];
    _pwdField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 40)];
    _pwdField.leftViewMode = UITextFieldViewModeAlways;
    _pwdField.background = FDImageWithName(@"Account_Field_Border");
    _pwdField.doneAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self moveBack];
    };
    _pwdField.editingAction = ^(NSString *text, UITextField *field) {
        StrongSelf;
        [s_self->_confirmButton setEnabled:text.length ? YES : NO];
    };
}

- (void)loadConstraints {
    
    [self.view addSubview:_backgroundView];
    [self.view addSubview:_forgetButton];
    [self.view addSubview:_forgetLine];
    [self.view addSubview:_forgetNoticeLabel];
    [self.view addSubview:_telField];
    [self.view addSubview:_smsField];
    [self.view addSubview:_pwdField];
    [self.view addSubview:_smsButton];
    [self.view addSubview:_confirmButton];
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
    }];
    
    [_telField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@(SCREEN_HEIGHT / 4));
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@40);
    }];
    
    if (_logicType == FDAccountLogicTypeLogin) {
        
        [_forgetButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(SCREEN_HEIGHT / 7));
            make.left.equalTo(@20);
            make.height.equalTo(@20);
        }];
        
        [_forgetLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_forgetButton.mas_bottom).offset(10);
            make.left.equalTo(_forgetButton);
            make.width.equalTo(_forgetButton);
            make.height.equalTo(@0.5);
        }];
    }
    
    if (_logicType == FDAccountLogicTypeForget) {
        
        [_forgetLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_telField.mas_top).offset(-25);
            make.left.right.equalTo(_telField);
            make.width.equalTo(_telField);
            make.height.equalTo(@0.5);
        }];
        
        [_forgetNoticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.bottom.equalTo(_forgetLine.mas_top).offset(-10);
        }];
    }
    
    if (_logicType == FDAccountLogicTypeRegister || _logicType == FDAccountLogicTypeForget) {
        
        [_smsField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_telField.mas_bottom).offset(10);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
            make.height.equalTo(@40);
        }];
        
        [_smsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.bottom.equalTo(_smsField);
            make.width.equalTo(@125);
        }];
    }
    
    [_pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_logicType == FDAccountLogicTypeLogin ? _telField.mas_bottom : _smsButton.mas_bottom).offset(10);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@40);
    }];
    
    [_confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_pwdField.mas_bottom).offset(30);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
        make.height.equalTo(@45);
    }];
}

- (void)loadSubViews {
    
    _backgroundView = [[UIImageView alloc] initWithImage:FDImageWithName(@"Account_Register_BG")];
    
    [self loadFields];
    
    NSString *confirmText = [self configureTitle];
    
    WeakSelf;
    
    _smsButton = [FDButton buttonWithType:UIButtonTypeCustom Title:@"发送验证码" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        if (![s_self checkIfTelLegal]) {
            return ;
        }
        
        [FDAccountService getSMSCodeWithParms:@{@"username" : s_self->_telField.text} Callback:^(BOOL success) {
            if (success) {
                [s_self->_timer setFireDate:[NSDate date]];
                [s_self->_smsButton setEnabled:NO];
            }
        }];
    }];
    
    [_smsButton setTitleColor:ColorAccountTextMain forState:UIControlStateNormal];
    
    
    
    _confirmButton = [FDButton buttonWithType:UIButtonTypeCustom Title:confirmText FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        
        if (s_self.logicType == FDAccountLogicTypeRegister) {
            
            if (![s_self checkIfTelLegal] || ![s_self checkIfPwdLegal] || ![s_self checkIfSMSCodeLegal]) {
                return ;
            }
            
            [FDAccountService registerWithParms:@{@"username" : s_self->_telField.text,
                                                  @"password" : s_self->_pwdField.text,
                                                  @"smscode"  : s_self->_smsField.text}
                                       Callback:^(BOOL success) {
                                           if (success) {
                                               return [s_self dismissViewControllerAnimated:YES completion:nil];
                                           }
                                       }];
            
        } else if (s_self.logicType == FDAccountLogicTypeLogin) {
            
            if (![s_self checkIfTelLegal] || ![s_self checkIfPwdLegal]) {
                return ;
            }
            
            [FDAccountService loginWithParms:@{@"username" : s_self->_telField.text,
                                               @"password" : [FDCoding md5:s_self->_pwdField.text]}
                                       Callback:^(BOOL success) {
                                           if (success) {
                                               return [s_self dismissViewControllerAnimated:YES completion:nil];
                                           }
                                       }];
        }
        
    }];
    
    [_confirmButton setBackgroundImageName:@"Account_Confirm_BG" AutoHighlight:NO AutoDisabled:YES AutoSelected:NO];
    [_confirmButton setTitleColor:ColorAccountTextMain forState:UIControlStateNormal];
    [_confirmButton setEnabled:NO];
    
    _forgetButton = [FDButton buttonWithType:UIButtonTypeSystem Title:@"忘记密码？" FontSize:18 ActionBlock:^(FDButton *button) {
        StrongSelf;
        FDAccountViewController *forgetViewController = [FDAccountViewController new];
        forgetViewController.logicType = FDAccountLogicTypeForget;
        [s_self.navigationController pushViewController:forgetViewController animated:YES];
    }];
    
    [_forgetButton setTitleColor:ColorAccountTextMain forState:UIControlStateNormal];
    
    _forgetLine = [UIView new];
    _forgetLine.backgroundColor = ColorNormalBGWhite;
    
    _forgetNoticeLabel = [UILabel labelWithText:@"请输入您的注册手机号\n以便我们给您发送验证码"
                                          Color:ColorNormalBGWhite
                                       FontSize:14
                                      Alignment:NSTextAlignmentCenter
                                          Light:NO];
    _forgetNoticeLabel.numberOfLines = 2;
    
    [self loadConstraints];
}

- (void)dealloc {
    [_timer invalidate];
}

#pragma mark - Helper

- (BOOL)checkIfTelLegal {
    
    if (![FDValidater validateMobile:_telField.text]) {
        [FDAlert alertWithTitle:@"错误" Message:@"手机号码格式不合法"];
        return NO;
    }
    
    return YES;
}

- (BOOL)checkIfPwdLegal {
    
    if (![FDValidater validatePassword:_pwdField.text]) {
        [FDAlert alertWithTitle:@"错误" Message:@"密码长度不合法"];
        return NO;
    }
    
    return YES;
}

- (BOOL)checkIfSMSCodeLegal {
    if (!_smsField.text.length) {
        [FDAlert alertWithTitle:@"错误" Message:@"短信验证码不能为空"];
        return NO;
    }
    
    return YES;
}

- (void)timerAction:(NSTimer *)timer {
    if (_interval >= 0) {
        [_smsButton setTitle:[NSString stringWithFormat:@"重新获取(%ld)", (long)_interval--] forState:UIControlStateNormal];
    } else {
        [_timer setFireDate:[NSDate distantFuture]];
        [_smsButton setEnabled:YES];
        [_smsButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        _interval = 60;
    }
}

- (void)moveHeigher {
    
    if (_isMoved) {
        return;
    }
    
    _isMoved = YES;
    
    CGRect rect = self.view.frame;
    
    rect.origin.y -= 50;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = rect;
    }];
}

- (void)moveBack {
    
    if (!_isMoved) {
        return;
    }
    
    _isMoved = NO;
    
    CGRect rect = self.view.frame;
    
    rect.origin.y += 50;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.view.frame = rect;
    }];
}

@end
