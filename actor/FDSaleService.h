//
//  FDSaleService.h
//  actor
//
//  Created by 王澍宇 on 16/4/4.
//  Copyright © 2016年 actopper. All rights reserved.
//

#import "FDWebService.h"
#import "FDBanner.h"
#import "FDAds.h"

@interface FDSaleService : FDWebService

+ (void)getBannersWithCallback:(void(^)(NSArray<FDBanner *> *banners, BOOL success))callback;

+ (void)getAdsWithCallback:(void(^)(FDAds *ads, BOOL success))callback;


@end
